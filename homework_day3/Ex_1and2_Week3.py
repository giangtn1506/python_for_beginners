
# Bài tập 1: Sets
A ={1, 2, 3, 4, 5, 7}
B ={2, 4, 5, 9, 12, 24}
C ={2, 4, 8}

for i in C:
    A.add(i)
print(A)

for i in C:
    B.add(i)
print(B)

D = A.intersection(B)
print(D)

E = A.difference(B)
print(E)

F = A.union(B)
print(F)

print(len(A))

print(len(B))

#vì F là union của A và B

print(max(F))
print(min(F))


# Bài tập 2: Tuples
t = 'python', [2, 3], [4,5]
type(t)

t1, t2, t3 = t


t21, t22 = t2
print("t21 = ", t21,"and", "t22 = ", t22)

t31, t32 = t3
print("t31 = ", t31,"and", "t32 = ", t32)

print("t after unpacked:  {", t1, t21, t22, t31, t32, "}" )

r = [2,3],
type(r)

# Ở đây ta thấy list [2,3] đã bị duplicated trong tuple mới

t = t[:3]
print(t)

t=list(t)
print(t)


