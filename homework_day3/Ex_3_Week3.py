#3.1
x = [1]
y = [1]

for i in range(2,16):
    x.append(i)

for i in range(2,16):
    y.append(i**0.5)

a = dict(zip(x,y))
print(a)

#3.2
#Sort an dictionary 's keys by their list
input = {'a': 5, 'b': 4, 'c': 10, 'd':0}
output_list = sorted(input, key = input.__getitem__)
print(output_list)

#3.3
s = 'Python is an easy language to learn'
l1 = list(s)
a2 = []
for i in l1:
    a2.append(l1.count(i))
dic_1 = dict(zip(l1,a2))
print(dic_1)

