# Excersie 1
u = 29
v = 12
x = 10
y = 4
z = 3
print(u/v)# ket qua = 2.4166666666
t = (u==v)
print(t) #t nhan gia tri false vi u khong bang v
print(u % x) # ket qua = 9 vi 29 chia 10 bang 2 du 9
t1 = (x>=y)
print(t1) #t1 nhan gia tri True vi x=10 >= y =4
u +=5
print(u) # u =34 vi u= u+5= 29+5= 34
u%=z
print(u) # u= 1 vi lay 34 chia 3 du 1 va gan gia tri 1 vao u
t = (v > x and y < z)
print(t) # t nhan gia tri false vi dieu kien and khong dc thoa man v>x nhung y>z
print(x**z) #ket qua = 1000 vi x**z = x^z = 10^3 = 1000
print(x // z) #ket qua = 3 vi phan nguyen cua thuong so trong phep chia la 3

# Excersie 2
#a
s = "Hi John, welcome to python programming for beginner!"
print("python" in s) #Ket qua true vi python co trong s
#b
s1 = s[3:7]
print(s1) #ket qua la chuoi "John"
#c
s2=s.split()
print(s2)
print(len(s2)) #Ket qua la 8


#Excersice 3
import math
r=5
P = math.pi*r*2
print('Chu vi hinh tron la', P) #Ket qua la 31.41
S= math.pi*r*r
print('Dien tich hinh tron la', S) #Ket qua la 78.53

#Excersice 4

s3="Twinkle, twinkle, little star, How I wonder what you are! Up above the world so high, Like a diamond in the sky. Twinkle, twinkle, little star, How I wonder what you are"
print(s3[0:30:1])
print("     ",s3[31:58:1])
print(s3[58:85:1])
print("    ",s3[85:111:1])
print(s3[112:142:1]) #O doan code nay em tu dong bi lui vao 1 dong ma ko ro li do. Thay co the giup em khong ah?
print("     ",s3[142:174:1]) #O day em cung gap tinh trang tuong tu nhu dong 50 a.

#Excersice 5
I = [23, 4.3, 4.2, 31, 'python', 1, 5.3, 9, 1.7]
I.remove('python')
print('Bo python =',I)
I.sort()
print('Sap theo thu tu nho toi lon =',I)
I.reverse()
print('Sap theo thu tu lon toi nho =',I)
a=4.2 in I
print('4.2 in I?=',a)






